// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDTsk7gRwCbTCj-_lqa8XhwQkCmFV2JMtw',
    authDomain: 'controle-011.firebaseapp.com',
    projectId: 'controle-011',
    storageBucket: 'controle-011.appspot.com',
    messagingSenderId: '476659979187',
    appId: '1:476659979187:web:4d40aac71b37176dd531c6',
    measurementId: 'G-0WGGZ8NFDB'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
